# Search Script for BASH

This script provides a simple CLI menu system for searching files within you filesystem

The default search path is /

To define a different search path, run this script with "./search [path]"

Search will include symlinks